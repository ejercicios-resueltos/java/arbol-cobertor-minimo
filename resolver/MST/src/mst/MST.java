package mst;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Clase que permite obtener un Árbol cobertor mínimo a partir de un grafo
 * @author Felipe
 */
public class MST {
    
    public int nodos;
    public int aristas;
    public ArrayList<Arista> aristasTodas;
    public ArrayList<Arista> [] aristasPorNodo;
    public boolean [] nodosRecorridos;
    public ArrayList<Arista> MST;
    
    /**
     * Encuentra el árbol cobertor mínimo (MST) del grafo leído
     */
    public void encontrarMST(){
        
    }
    
    /**
     * Entrega por salida estándar la suma de pesos del MST
     */
    public void printPesosMST(){
        
    }
    
    /**
     * Entrega por salida estándar las aristas que forman el MST.
     */
    public void printMST(){
        
    }
    
    /**
     * Entrega por salida estándar todas las aristas que conforman el grafo.
     */
    public void printAristasGrafo(){
        for (int v = 0; v < nodos; v++){
            System.out.println("" + v + " -> " + aristasPorNodo[v]);
        }
    }
    
    /**
     * Lee el archivo indicado y guarda los datos en la estructura
     * @param nombreArchivo nombre del archivo que contiene los datos
     */
    public void leerArchivo(String nombreArchivo) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        try {
            archivo = new File(nombreArchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // ---- Lectura del fichero ----
            
            // --- FIN Lectura del fichero ---
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}

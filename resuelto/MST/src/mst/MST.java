package mst;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Clase que permite obtener un Árbol cobertor mínimo a partir de un grafo
 * @author Felipe
 */
public class MST {
    
    public int nodos;
    public int aristas;
    public ArrayList<Arista> aristasTodas;
    public ArrayList<Arista> [] aristasPorNodo;
    public boolean [] nodosRecorridos;
    public ArrayList<Arista> MST;
    
    /**
     * Encuentra el árbol cobertor mínimo (MST) del grafo leído
     */
    public void encontrarMST(){
        ArrayList<Arista> siguientes = this.aristasPorNodo[0];
        Collections.sort(siguientes);
        Arista a = siguientes.get(0);
        this.nodosRecorridos[a.origen] = true;
        this.nodosRecorridos[a.destino] = true;
        this.MST.add(a);
        
        boolean fin;
        while (true)
        {
            siguientes = new ArrayList();
            fin = true;
            for (int v = 0; v < nodos; v++){
                if (!this.nodosRecorridos[v])
                    fin = false;
                else{
                    siguientes.addAll(this.aristasPorNodo[v]);
                }
            }
            if (fin) return;
            
            Collections.sort(siguientes);
            for (int i = 0; i < siguientes.size(); i++){
                a = siguientes.get(i);
                if (!(this.nodosRecorridos[a.origen] && this.nodosRecorridos[a.destino])){
                    this.nodosRecorridos[a.origen] = true;
                    this.nodosRecorridos[a.destino] = true;
                    this.MST.add(a);
                    break;
                }
            }
        }
    }
    
    /**
     * Entrega por salida estándar la suma de pesos del MST
     */
    public void printPesosMST(){
        int suma = 0;
        for (Arista a : this.MST)
        {
            suma += a.peso;
        }
        System.out.println(suma);
    }
    
    /**
     * Entrega por salida estándar las aristas que forman el MST.
     */
    public void printMST(){
        if (this.MST.isEmpty())
        {
            this.encontrarMST();
        }
        Collections.sort(MST);
        for (Arista a : this.MST)
        {
            a.printValores();
        }
    }
    
    /**
     * Entrega por salida estándar todas las aristas que conforman el grafo.
     */
    public void printAristasGrafo(){
        for (int v = 0; v < nodos; v++){
            System.out.println("" + v + " -> " + aristasPorNodo[v]);
        }
    }
    
    /**
     * Lee el archivo indicado y guarda los datos en la estructura
     * @param nombreArchivo nombre del archivo que contiene los datos
     */
    public void leerArchivo(String nombreArchivo) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;

        try {
            archivo = new File(nombreArchivo);
            fr = new FileReader(archivo);
            br = new BufferedReader(fr);

            // ---- Lectura del fichero ----
            String linea;
            linea = br.readLine();
            String [] datos = linea.split(" ");
            this.nodos = Integer.parseInt(datos[0]);
            this.aristas = Integer.parseInt(datos[1]);
            this.aristasPorNodo = new ArrayList[nodos];
            this.nodosRecorridos = new boolean[nodos];
            this.MST = new ArrayList();
            for (int v = 0; v < nodos; v++){
                this.aristasPorNodo[v] = new ArrayList();
            }
            this.aristasTodas = new ArrayList(aristas);
            
            int a = 0;
            int origen, destino;
            int peso;
            while ((linea = br.readLine()) != null && a<aristas) {
                datos = linea.split(" ");
                origen = Integer.parseInt(datos[0]);
                destino = Integer.parseInt(datos[1]);
                peso = Integer.parseInt(datos[2]);
                Arista arista = new Arista(origen, destino, peso);
                this.aristasPorNodo[origen].add(arista);
                this.aristasPorNodo[destino].add(arista);
                this.aristasTodas.add(arista);
                a++;
            }
            // --- FIN Lectura del fichero ---
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fr) {
                    fr.close();
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
